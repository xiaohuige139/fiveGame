var bak=new Image();
bak.src='./images/bak.jpg';
var black=new Image();
black.src='./images/black.png';
var white=new Image();
white.src='./images/white.png';

//初始数组
var piece=[];
for(var x=0;x<16;x++){
	piece[x]=[];
	for(var y=0;y<16;y++){
	piece[x][y]=0;
	}
}
var flag=true;
//重新开始
function bg(context){
    context.drawImage(bak,0,0,600,600);
    for(var i=0;i<15;i++){
        for(var j=0;j<15;j++){
            context.strokeStyle='#fff';
            context.strokeRect(i*40,j*40,40,40);
        }
    }
    for(x=0;x<16;x++){
        piece[x]=[];
        for(y=0;y<16;y++){
            piece[x][y]=0;
        }
    }
}

//新建元素
function crea_ele(msg00){
    var liObj = document.createElement('li');
    document.documentElement.appendChild(liObj);
    ul0101.appendChild(liObj);
    liObj.style.cssText='width:700px;height:30px;';
    var time_msg=timemsg();
    liObj.innerText=msg00+"比赛结束时间："+time_msg;
}

//清除游戏记录
function remove_ele(){
	while(ul0101.hasChildNodes()){
		ul0101.removeChild(ul0101.firstChild);
	}
}
//获取时间
function timemsg(){
    var time1=new Date();
        var h=time1.getHours();
        var mi=time1.getMinutes();
        var ss=time1.getSeconds();
        if(h>=0&&h<=9){
            return "0"+h+':'+mi+':'+ss;
        }
        else if(mi>=0&&mi<=9){
            return h+':'+"0"+mi+':'+ss;
        }
        else if(ss>=0&&ss<=9){
            return h+':'+mi+':'+"0"+ss;
        }else{
            return h+':'+mi+':'+ss;
        }
}
window.onload=function(){
	var canvas = document.getElementById("canvas");
    var btn = document.getElementById("btn");
    var re = document.getElementById("re");
    var ul0101 = document.getElementById("ul0101");
    var context = canvas.getContext("2d");
    bg(context);
    //点击重新开始游戏
    btn.onclick=function(){
        bg(context);
    }
    //点击清除游戏记录
    re.onclick=function(){
        remove_ele();
    }
    //点击下棋
    canvas.onclick=function(event){
        play(event,canvas,context);
    }
}



function play(ev,canvas,context){
    var mx=ev.clientX-canvas.offsetLeft;
    var my=ev.clientY-canvas.offsetTop;

    //获取棋子的位置
    var x=positionFun(mx);
    var y=positionFun(my);

    var row=(x==0)?0:x/40;
    var cos=(y==0)?0:y/40;
    //判断有没有棋子
    if(piece[row][cos]!=0){
        return false;
    }

    if(flag){
        context.drawImage(white,x-18,y-18);
        piece[row][cos]=1;
        flag=!flag;
    }
    else{
        context.drawImage(black,x-18,y-18);
        piece[row][cos]=2;
        flag=!flag;
    }

    //判断胜利
    if(getResult(row,cos)){
        var msg00
        if(flag==false){
            msg00="白棋胜利,黑棋失败！";
            crea_ele(msg00);
            alert('白棋胜利');
        }else{
            msg00="黑棋胜利,白棋失败！";
            crea_ele(msg00);
            alert('黑棋胜利');
        }
    }
}
//获取棋子的位置
function positionFun(val){
    return Math.round(val/40)*40;
}

//判断胜利
function getResult(row,cos){
    var countx=1;
    var county=1;
    var countxy=1;
    var countyx=1;
    //判断横
    for(var i=0;i<15;i++){
        if(piece[i][cos]==piece[i+1][cos]&&piece[i][cos]!=0){
            countx++;
            if(countx==5){
                return 1;
            }
        }else{
            countx=1;
        } 
    }
    //判断竖
    for(var j=0;j<15;j++){
        if(piece[row][j]==piece[row][j+1]&&piece[row][j]!=0){
            county++;
            if(county==5){
                return 1;
            }
        }else{
            county=1;
        }     
    }
    //判断斜线左上到右下方向
    for(var k=row,l=cos;k>0&&l>0;k--,l--){
        if(piece[k][l]==piece[k-1][l-1]&&piece[k][l]!=0){
            countxy++;
            if(countxy==5){
                return 1;
            }
        }else{
            break;
        }     
    }
    for(k=row,l=cos;k<15&&l<15;k++,l++){
        if(piece[k][l]==piece[k+1][l+1]&&piece[k][l]!=0){
            countxy++;
            if(countxy==5){
                return 1;
            }
        }else{
            break;
        }     
    }

    //判断斜线右上到左下方向
    for(k=row,l=cos;k<15&&l>0;k++,l--){
        if(piece[k][l]==piece[k+1][l-1]&&piece[k][l]!=0){
            countyx++;
            if(countyx==5){
                return 1;
            }
        }else{
            break;
        }     
    }
    for(k=row,l=cos;k>0&&l<15;k--,l++){
        if(piece[k][l]==piece[k-1][l+1]&&piece[k][l]!=0){
            countyx++;
            if(countyx==5){
                return 1;
            }
        }else{
            break;
        }     
    }
    
}